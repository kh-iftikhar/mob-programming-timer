package no.hassan.intellij.plugins.mobtimer.history;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayOutputStream;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.tools.javac.util.List;
import no.hassan.intellij.plugins.mobtimer.Mobber;
import no.hassan.intellij.plugins.mobtimer.Model;
import org.junit.jupiter.api.Test;

class JsonExporterTests {

  private Exporter exporter = new JsonExporter();

  @Test
  void exportsCorrectly() {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    Mobber loadedMobber = new Mobber("Loaded");
    loadedMobber.setSkipCount(10);
    loadedMobber.setTurnCount(200);
    List<Mobber> mobbers = List.of(new Mobber("A"), new Mobber("B"), loadedMobber);
    Model model = new Model(15, 10_00_00, mobbers);
    boolean exported = exporter.export(stream, model);
    String data = stream.toString();
    assertFalse(data == null || data.trim().isEmpty());
    JsonObject json = new JsonParser().parse(data).getAsJsonObject();
    Model parsedModel = new Model(json);
    assertAll(() -> assertTrue(exported), () -> assertEquals(model, parsedModel));
  }
}
