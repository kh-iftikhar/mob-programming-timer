package no.hassan.intellij.plugins.mobtimer.history;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import no.hassan.intellij.plugins.mobtimer.Model;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

public class JsonImporter implements Importer {

  @Override
  public Model load(@NotNull Path mobFilePath) {
    try {
      String data = new String(Files.readAllBytes(mobFilePath), Charset.defaultCharset());
      JsonObject modelJson = new JsonParser().parse(data).getAsJsonObject();
      return new Model(modelJson);
    } catch (IOException e) {
      logger.warn("Unable to load file", e);
      return Importer.getDefault();
    }
  }
}
