package no.hassan.intellij.plugins.mobtimer.actions;

import com.intellij.openapi.project.Project;
import no.hassan.intellij.plugins.mobtimer.MobbingManager;
import org.jetbrains.annotations.NotNull;

import javax.swing.JTextField;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.MSG_INVALID_NAME;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.TITLE_ERROR;

public class AddAction extends AbstractAction {

  private final JTextField nameField;

  public AddAction(
      @NotNull Project project,
      @NotNull MobbingManager mobbingManager,
      @NotNull JTextField nameField) {
    super(project, mobbingManager);
    this.nameField = nameField;
    this.nameField.addKeyListener(
        new KeyAdapter() {
          @Override
          public void keyReleased(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
              performAdd();
            }
          }
        });
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    performAdd();
  }

  private void performAdd() {
    String name = nameField.getText();
    if (name == null || name.trim().length() == 0 || name.contains(":")) {
      showInfoMessage(TITLE_ERROR, MSG_INVALID_NAME);
    } else {
      manager.addMobber(name);
      nameField.setText(null);
    }
  }
}
