package no.hassan.intellij.plugins.mobtimer.history;

import com.intellij.openapi.diagnostic.Logger;
import no.hassan.intellij.plugins.mobtimer.Model;
import org.jetbrains.annotations.NotNull;

import java.io.OutputStream;

public interface Exporter {

  Logger logger = Logger.getInstance(Exporter.class);

  boolean export(@NotNull OutputStream output, @NotNull Model model);
}
