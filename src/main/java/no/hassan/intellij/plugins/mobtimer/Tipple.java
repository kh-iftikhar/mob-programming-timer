package no.hassan.intellij.plugins.mobtimer;

import java.util.Objects;

public class Tipple<P1, P2, P3> {

  public final P1 p1;
  public final P2 p2;
  public final P3 p3;

  private Tipple(P1 p1, P2 p2, P3 p3) {
    this.p1 = p1;
    this.p2 = p2;
    this.p3 = p3;
  }

  public static <P1, P2, P3> Tipple<P1, P2, P3> of(P1 p1, P2 p2, P3 p3) {
    return new Tipple<>(p1, p2, p3);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Tipple<?, ?, ?> tipple = (Tipple<?, ?, ?>) o;
    return p1.equals(tipple.p1) && p2.equals(tipple.p2) && p3.equals(tipple.p3);
  }

  @Override
  public int hashCode() {
    return Objects.hash(p1, p2, p3);
  }

  @Override
  public String toString() {
    return "Tipple{" + "p1=" + p1 + ", p2=" + p2 + ", p3=" + p3 + '}';
  }
}
