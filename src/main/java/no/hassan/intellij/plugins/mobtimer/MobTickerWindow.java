package no.hassan.intellij.plugins.mobtimer;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.util.ui.JBImageIcon;
import no.hassan.intellij.plugins.mobtimer.actions.AbstractAction;
import no.hassan.intellij.plugins.mobtimer.actions.AddAction;
import no.hassan.intellij.plugins.mobtimer.actions.ClearHistoryAction;
import no.hassan.intellij.plugins.mobtimer.actions.ExportAction;
import no.hassan.intellij.plugins.mobtimer.actions.PauseResumeAction;
import no.hassan.intellij.plugins.mobtimer.actions.RemoveAction;
import no.hassan.intellij.plugins.mobtimer.actions.ShuffleAction;
import no.hassan.intellij.plugins.mobtimer.actions.StartAction;
import no.hassan.intellij.plugins.mobtimer.actions.StopAction;
import no.hassan.intellij.plugins.mobtimer.actions.TimerAction;
import no.hassan.intellij.plugins.mobtimer.actions.UpdateAction;
import org.jetbrains.annotations.NotNull;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import java.util.Objects;
import java.util.Vector;

public class MobTickerWindow {

  private static final String BASE_NAME = "mobing";

  private JPanel mainPanel;
  private JLabel lblTicker;
  private JTable tblParticipants;
  private JTextField txtAddParticipants;
  private JButton btnAdd;
  private JButton btnRemove;
  private JButton btnPause;
  private JButton btnStart;
  private JButton btnStop;

  @SuppressWarnings("unused")
  private JPanel pnlControls;

  @SuppressWarnings("unused")
  private JPanel pnlTop;

  private JTextField txtIntervalLength;
  private JButton btnUpdateInterval;

  @SuppressWarnings("unused")
  private JLabel lblTickLength;

  @SuppressWarnings("unused")
  private JScrollPane tblPane;

  private JLabel lblMoberName;
  private JButton btnRearrange;

  @SuppressWarnings("unused")
  private JPanel pnlFront;

  @SuppressWarnings("unused")
  private JLabel lblTotalMobTime;

  private JLabel lblSessionMobTime;
  private JButton btnExport;
  private JButton btnClear;

  MobTickerWindow(ToolWindow toolWindow, @NotNull Project project) {
    initializeGUI(toolWindow, project);
  }

  JPanel getContent() {
    return mainPanel;
  }

  private void initializeGUI(ToolWindow toolWindow, @NotNull Project project) {
    String mode = System.getProperty("mode", "prod");
    MobbingManager manager = new MobbingManager(project, mode);
    ApplicationManager.getApplication().addApplicationListener(manager, project);
    ProjectManager projectManager = ProjectManager.getInstance();
    projectManager.addProjectManagerListener(project, manager);
    txtIntervalLength.setText(String.valueOf(manager.getIntervalLength()));
    // Setup all the buttons
    configureButton(btnStart, "start", new StartAction(project, manager, tblParticipants));
    configureButton(btnPause, "pause", new PauseResumeAction(project, manager, btnPause));
    configureButton(
        btnUpdateInterval, "update", new UpdateAction(project, manager, txtIntervalLength));
    configureButton(btnRearrange, "shuffle", new ShuffleAction(project, manager));
    configureButton(btnStop, "stop", new StopAction(project, manager));
    configureButton(btnAdd, "add", new AddAction(project, manager, txtAddParticipants));
    configureButton(btnRemove, "remove", new RemoveAction(project, manager, tblParticipants));
    configureButton(btnClear, "clear", new ClearHistoryAction(project, manager));
    configureButton(btnExport, "export", new ExportAction(project, manager));
    new TimerAction(project, manager, lblTicker, lblTotalMobTime, lblSessionMobTime, lblMoberName);
    toolWindow.setTitle("Mob Programming Timer");
    Vector<String> columnNamesVector = new Vector<>(4);
    columnNamesVector.add("Mobbers");
    columnNamesVector.add("Turns");
    columnNamesVector.add("Skips");
    columnNamesVector.add("Gone");

    MobbersTableModel tableModel = new MobbersTableModel(manager, columnNamesVector);
    for (int i = 0; i < tableModel.getColumnCount(); i++) {
      tblParticipants.setDefaultRenderer(Object.class, new ParticipantRowRenderer(manager));
    }
    tblParticipants.setModel(tableModel);
    tblParticipants.setRowSelectionAllowed(true);
  }

  private <A extends AbstractAction> void configureButton(
          @NotNull JButton btn, @NotNull String iconName, @NotNull A action) {
    try {
      btn.addActionListener(action);
      btn.setIcon(
          new JBImageIcon(
              ImageIO.read(
                  Objects.requireNonNull(
                      getClass()
                          .getClassLoader()
                          .getResourceAsStream(String.format("icons/%s.png", iconName))))));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
