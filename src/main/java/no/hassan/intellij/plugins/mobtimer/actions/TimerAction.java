package no.hassan.intellij.plugins.mobtimer.actions;

import com.intellij.icons.AllIcons;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import no.hassan.intellij.plugins.mobtimer.Constants;
import no.hassan.intellij.plugins.mobtimer.MobUtils;
import no.hassan.intellij.plugins.mobtimer.Mobber;
import no.hassan.intellij.plugins.mobtimer.MobbingManager;
import no.hassan.intellij.plugins.mobtimer.Pair;
import no.hassan.intellij.plugins.mobtimer.Tipple;
import org.jetbrains.annotations.NotNull;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.JLabel;
import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.InputStream;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static no.hassan.intellij.plugins.mobtimer.Constants.Durations.ONE_SECOND;
import static no.hassan.intellij.plugins.mobtimer.Constants.LeavingMessages;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.CHANGED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.CLEAR;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.EXPORTED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.PAUSED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.RESTARTED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.RESUMED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.STOPPED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.UNAVAILABLE;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.UPDATED;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.BTN_GOT_IT;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.BTN_SKIP;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.MSG_THIS_SESSION;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.MSG_TOTAL_SESSION;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.MSG_UNAVAILABLE;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.TITLE_ERROR;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.TITLE_SWITCH;
import static no.hassan.intellij.plugins.mobtimer.Constants.WelcomeMessages;

public class TimerAction extends AbstractAction implements PropertyChangeListener {

  private static final Logger logger = Logger.getInstance(TimerAction.class);
  private static final String ALERT_SOUND = "/sounds/rain.wav";

  private static final long DEFAULT_SCREAM_LIMIT = TimeUnit.MINUTES.toMinutes(2);
  private final StringBuilder builder = new StringBuilder();
  private final Timer timer;
  private final JLabel lblTicker;
  private final JLabel lblTotalMobTime;
  private final JLabel lblSessionMobTime;
  private final JLabel lblMobberName;
  private long screamLimit = DEFAULT_SCREAM_LIMIT;
  private int minutesPerParticipant;
  private Duration duration;
  private long activeSessionTimeInMillis = 0;
  private Clip breakClip;

  public TimerAction(
      @NotNull Project project,
      @NotNull MobbingManager mobbingManager,
      @NotNull JLabel lblTicker,
      @NotNull JLabel lblTotalMobTime,
      @NotNull JLabel lblSessionMobTime,
      @NotNull JLabel lblMobberName) {
    super(project, mobbingManager);
    this.lblMobberName = lblMobberName;
    mobbingManager.addPropertyChangeListener(this);
    timer = new Timer(ONE_SECOND, this);
    this.lblTicker = lblTicker;
    this.lblTotalMobTime = lblTotalMobTime;
    this.lblSessionMobTime = lblSessionMobTime;
    minutesPerParticipant = mobbingManager.getIntervalLength();
    duration = Duration.ofMinutes(minutesPerParticipant);
    breakClip = setupBreakClip();
    updateTickerTimings();
    updateTotalMobTiming();
    if (mobbingManager.getMode().equals(Constants.Mode.DEV)) {
      screamLimit = TimeUnit.SECONDS.toMillis(10);
    }
  }

  private Clip setupBreakClip() {
    try {
      InputStream stream = getClass().getClassLoader().getResourceAsStream(ALERT_SOUND);
      if (stream == null) {
        return null;
      }
      AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(stream);
      Clip clip = AudioSystem.getClip();
      clip.open(audioInputStream);
      return clip;
    } catch (Exception e) {
      logger.error("Unable to open sound resource", e);
      return null;
    }
  }

  private void startScreaming(long forMillis) {
    startScreaming();
    long timeToStop = System.currentTimeMillis() + forMillis;
    new Thread(
            () -> {
              while (System.currentTimeMillis() <= timeToStop) {
                try {
                  Thread.sleep(100);
                } catch (InterruptedException e) {
                  stopScreaming();
                }
              }
              stopScreaming();
            })
        .start();
  }

  private void startScreaming() {
    try {
      breakClip.setMicrosecondPosition(0);
      breakClip.start();
      breakClip.loop(Clip.LOOP_CONTINUOUSLY);
    } catch (Exception e) {
      logger.error("Unable to start sound", e);
    }
  }

  private void stopScreaming() {
    try {
      if (breakClip.isRunning()) {
        breakClip.stop();
      }
    } catch (Exception e) {
      logger.error("Unable to stop sound", e);
    }
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (duration.toMillis() == 0) {
      startScreaming(screamLimit);
      manager.countdownFinished();
    } else {
      duration = duration.minusSeconds(1);
    }
    activeSessionTimeInMillis += ONE_SECOND;
    manager.addToTotalMobTiming(ONE_SECOND);
    updateTickerTimings();
    updateActiveSessionTiming();
    updateTotalMobTiming();
  }

  private void start(Mobber oldValue, Mobber newValue) {
    if (timer.isRunning()) {
      timer.stop();
    }
    if (oldValue != null && newValue != null) {
      String fmt = LeavingMessages.get((int) (Math.random() * LeavingMessages.size()));
      int nowWhat =
          Messages.showOkCancelDialog(
              project,
              String.format(fmt, oldValue.getName(), newValue.getName()),
              TITLE_SWITCH,
              BTN_GOT_IT,
              BTN_SKIP,
              AllIcons.General.Information);
      if (nowWhat != Messages.OK) {
        manager.skipCurrentMobber(newValue);
        return;
      }
    }
    stopScreaming();
    if (newValue != null) {
      lblMobberName.setText(newValue.getName());
      duration = Duration.ofMinutes(minutesPerParticipant);
      timer.start();
      String fmt = WelcomeMessages.get((int) (Math.random() * WelcomeMessages.size()));
      lblMobberName.setText(String.format(fmt, newValue.getName()));
    }
  }

  private void pause() {
    if (timer.isRunning()) {
      timer.stop();
    }
  }

  private void resume() {
    if (!timer.isRunning()) {
      timer.restart();
    }
  }

  private void restart() {
    if (timer.isRunning()) {
      timer.stop();
    }
    stopScreaming();
    duration = Duration.ofMinutes(minutesPerParticipant);
    updateTickerTimings();
  }

  private void stop() {
    if (timer.isRunning()) {
      timer.stop();
    }
    stopScreaming();
    duration = Duration.ofMinutes(minutesPerParticipant);
    lblMobberName.setText(null);
    lblTicker.setText("00:00");
    lblSessionMobTime.setText("00:00");
    activeSessionTimeInMillis = 0;
  }

  private void updateTickerTimings() {
    Pair<Integer, Byte> minutesAndSeconds = MobUtils.toMinutesAndSeconds(duration.toMillis());
    lblTicker.setText(
        (minutesAndSeconds.p1 < 10 ? "0" + minutesAndSeconds.p1 : minutesAndSeconds.p1)
            + ":"
            + (minutesAndSeconds.p2 < 10 ? "0" + minutesAndSeconds.p2 : minutesAndSeconds.p2));
  }

  private void updateActiveSessionTiming() {
    builder.setLength(0);
    Pair<Long, Long> hoursAndMinutes =
        MobUtils.toHoursAndMinutes(activeSessionTimeInMillis / ONE_SECOND);
    Long hours = hoursAndMinutes.p1;
    Long minutes = hoursAndMinutes.p2;
    builder.append(MSG_THIS_SESSION);
    appendHoursAndMinutes(builder, hours, minutes);
    lblSessionMobTime.setText(builder.toString());
    builder.setLength(0);
  }

  private void updateTotalMobTiming() {
    builder.setLength(0);
    long seconds = manager.getTotalMobTiming();
    Tipple<Long, Long, Long> daysHoursAndMinutes = MobUtils.toDaysHoursAndMinutes(seconds);
    Long days = daysHoursAndMinutes.p1;
    Long hours = daysHoursAndMinutes.p2;
    Long minutes = daysHoursAndMinutes.p3;
    builder.append(MSG_TOTAL_SESSION);
    if (days > 0) {
      if (days < 10) {
        builder.append("0");
      }
      builder.append(days).append(":");
    }
    appendHoursAndMinutes(builder, hours, minutes);
    lblTotalMobTime.setText(builder.toString());
    builder.setLength(0);
  }

  private void appendHoursAndMinutes(StringBuilder builder, long hours, long minutes) {
    if (hours < 10) {
      builder.append("0");
    }
    builder.append(hours).append(":");
    if (minutes < 10) {
      builder.append("0");
    }
    builder.append(minutes);
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    switch (evt.getPropertyName()) {
      case CHANGED:
        start((Mobber) evt.getOldValue(), (Mobber) evt.getNewValue());
        break;
      case PAUSED:
        pause();
        break;
      case RESUMED:
        resume();
        break;
      case RESTARTED:
        restart();
        break;
      case STOPPED:
        stop();
        break;
      case EXPORTED:
        updateTickerTimings();
        updateTotalMobTiming();
      case UPDATED:
        if (minutesPerParticipant != manager.getIntervalLength()) {
          minutesPerParticipant = manager.getIntervalLength();
          duration = Duration.ofMinutes(minutesPerParticipant);
          updateTickerTimings();
        }
        break;
      case UNAVAILABLE:
        stopScreaming();
        showInfoMessage(TITLE_ERROR, MSG_UNAVAILABLE);
        stop();
        break;
      case CLEAR:
        stopScreaming();
        if (timer.isRunning()) {
          timer.stop();
        }
        minutesPerParticipant = manager.getIntervalLength();
        duration = Duration.ofMinutes(minutesPerParticipant);
        activeSessionTimeInMillis = 0;
        lblMobberName.setText(null);
        updateTickerTimings();
        updateActiveSessionTiming();
        updateTotalMobTiming();
        break;
    }
  }
}
