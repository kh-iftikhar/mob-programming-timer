package no.hassan.intellij.plugins.mobtimer.actions;

import com.intellij.openapi.project.Project;
import com.intellij.util.ui.JBImageIcon;
import no.hassan.intellij.plugins.mobtimer.MobbingManager;
import org.jetbrains.annotations.NotNull;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import static java.util.Objects.requireNonNull;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.CHANGED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.CLEAR;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.PAUSED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.RESTARTED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.RESUMED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.STOPPED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.UNAVAILABLE;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.BTN_PAUSE;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.BTN_RESUME;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.MSG_PAUSE;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.TITLE_ERROR;

public class PauseResumeAction extends AbstractAction implements PropertyChangeListener {

  private final JButton btnPause;

  public PauseResumeAction(
          @NotNull Project project, @NotNull MobbingManager mobbingManager, JButton btnPause) {
    super(project, mobbingManager);
    mobbingManager.addPropertyChangeListener(this);
    this.btnPause = btnPause;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (manager.getCurrentMobber().isPresent()) {
      boolean running = manager.isRunning();
      manager.setRunning(!running);
    } else {
      showInfoMessage(TITLE_ERROR, MSG_PAUSE);
    }
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (evt != null) {
      switch (evt.getPropertyName()) {
        case RESTARTED:
        case RESUMED:
        case STOPPED:
        case UNAVAILABLE:
        case CLEAR:
        case CHANGED:
          btnPause.setText(BTN_PAUSE);
          try {
            btnPause.setIcon(
                new JBImageIcon(
                    ImageIO.read(
                        requireNonNull(
                            getClass().getClassLoader().getResourceAsStream("icons/pause.png")))));
          } catch (Exception ignored) {
          }
          break;
        case PAUSED:
          btnPause.setText(BTN_RESUME);
          try {
            btnPause.setIcon(
                new JBImageIcon(
                    ImageIO.read(
                        requireNonNull(
                            getClass().getClassLoader().getResourceAsStream("icons/resume.png")))));
          } catch (Exception ignored) {
          }
          break;
      }
    }
  }
}
