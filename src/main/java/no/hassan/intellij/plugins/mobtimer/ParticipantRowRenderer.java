package no.hassan.intellij.plugins.mobtimer;

import com.intellij.ui.JBColor;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;

public class ParticipantRowRenderer implements TableCellRenderer {

  private static final DefaultTableCellRenderer DEFAULT_RENDERER = new DefaultTableCellRenderer();

  private final MobbingManager model;

  ParticipantRowRenderer(MobbingManager mobbingManager) {
    model = mobbingManager;
  }

  @Override
  public Component getTableCellRendererComponent(
          JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    Component c =
        DEFAULT_RENDERER.getTableCellRendererComponent(
            table, value, isSelected, hasFocus, row, column);
    if (model.getCurrentMobberIndex() == row) {
      c.setBackground(JBColor.ORANGE);
    } else {
      c.setBackground(JBColor.WHITE);
    }
    if (isSelected) {
      c.setForeground(JBColor.BLACK);
    }
    return c;
  }
}
